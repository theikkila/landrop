#include "sql_controller.h"
	
sql::Database::Database(){
	db = QSqlDatabase::addDatabase("QSQLITE");
	QString path(QDir::home().path());
	path.append(QDir::separator()).append("landrop.db.sqlite");
	path = QDir::toNativeSeparators(path);
	db.setDatabaseName(path);
}

sql::Database::~Database(){
	db.close();
}

bool sql::Database::open(){
	entrycount = 0;
	if(!db.isOpen()){
		return db.open();
	}
	return true;
}

void sql::Database::close(){
	db.close();
}

bool sql::Database::init(){
	bool ret = false;
	if(db.isOpen()){
		QSqlQuery query;
		ret = query.exec("create table if not exists files (id integer primary key, type integer, filename varchar(120), path varchar(1000), timestamp datetime, hash integer)");
		if(!ret){
			std::cout << query.lastError().text().toStdString() << std::endl;
		}
	}
	return ret;
}

int sql::Database::addFile(const int &type, const std::string &filename, const std::string &path, const std::string &timestamp, const quint16 &hash){
	int newId = -1;
	bool ret = false;
	const QString qfilename = QString(filename.c_str());
	const QString qpath = QString(path.c_str());
	const QString qtimestamp = QString(timestamp.c_str());
	if (db.isOpen()){
		int id = fileAlreadyInDatabase(type, filename, path);
		if(id == -1){
			QSqlQuery query;
			ret = query.exec(QString("insert into files values(NULL,%1,'%2','%3','%4',%5)").arg(type).arg(qfilename).arg(qpath).arg(qtimestamp).arg(hash));
			if (ret){
				newId = query.lastInsertId().toInt();
				entrycount++;
			}else{
				std::cout << query.lastError().text().toStdString() << std::endl;
			}
		}else{
			std::cout << "timestamp in db: " << getTimestamp(id) << " and for real: " << timestamp << std::endl;
			if(getTimestamp(id) != timestamp){
				std::cout << "update" << std::endl;
				updateHash(id, timestamp, hash);	
			}
		}
	}
	return newId;
}

int sql::Database::fileAlreadyInDatabase(const int &type, const std::string &filename, const std::string &path){
	const QString qfilename = QString(filename.c_str());
	const QString qpath = QString(path.c_str());
	int ret = -1;
	if(!db.isOpen()){
		return ret;
	}
	QSqlQuery query;
	query.exec(QString("select * from files where type = %1 and filename = '%2' and path = '%3'").arg(type).arg(qfilename).arg(qpath));
	if(query.next()){
		ret = query.value(0).toInt();
	}
	return ret;
}

bool sql::Database::updateHash(const int &id, const std::string &timestamp, const quint16 &hash){
	const QString qtimestamp = QString(timestamp.c_str());
	bool ret = false;
	if(!db.isOpen()){
		std::cout << "ohnoes db is no more open" << std::endl;
		return ret;
	}
	QSqlQuery query;
	query.exec(QString("update files set timestamp = '%1', hash = %2 where id = %3").arg(qtimestamp).arg(hash).arg(id));
	std::cout << query.lastError().text().toStdString() << std::endl;
	return ret;
}

std::string sql::Database::getTimestamp(const int &id){
	std::string ret = "-";
	if(!db.isOpen()){
		return ret;
	}
	QSqlQuery query;
	query.exec(QString("select timestamp from files where id = %1").arg(id));
	if(query.next()){
		ret = query.value(0).toString().toStdString();
	}
	return ret;
}

long int sql::Database::getEntryCount(){
	return entrycount;
}

std::string sql::Database::lastError(){
	return db.lastError().text().toStdString();
}