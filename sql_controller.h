/*
Sql-controller for database
*/
#ifndef SQL_CONTROLLER_H
#define SQL_CONTROLLER_H

#include <QtSql>
#include <iostream>

namespace sql{
	
class Database{
public:
	Database();
	~Database();
public:
	bool open();
	void close();
	bool init();
	int addFile(const int &type, const std::string &filename, const std::string &path, const std::string &timestamp, const quint16 &hash);
	int fileAlreadyInDatabase(const int &type, const std::string &filename, const std::string &path);
	bool updateHash(const int &id, const std::string &timestamp, const quint16 &hash);
	std::string getTimestamp(const int &id);
	long int getEntryCount();
	std::string lastError();
private:
	QSqlDatabase db;
	long int entrycount;
};

}

#endif