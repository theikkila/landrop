/*
* Headers of filemapper.
* Filemapper populates database with files and calculates file fingerprints
* Filemapper also trigger events when something changes.
*/
#ifndef FILEMAPPER_H
#define FILEMAPPER_H

#include "sql_controller.h"
#include <QDateTime>
#include <QByteArray>
#include <QFile>
#include <iostream>

namespace file{
	
	enum Type{FILE, FOLDER};
	
class Filemapper{
public:
	Filemapper(const std::string &path, sql::Database *db);
	void searchAndPopulate(const std::string &path);
	/*
	void searchForChanges(std::string &path);
	bool changed(std::string &path);
	timestamp getTimestamp(std::string &file);
	*/
	quint16 getHash(const std::string &path);
private:
	QDir root_;
	sql::Database *dbptr;
};
}

#endif