/****************************************************************************
** Meta object code from reading C++ file 'service_provider.h'
**
** Created: Sun Feb 5 21:17:49 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "service_provider.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'service_provider.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_network__ServiceThread[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_network__ServiceThread[] = {
    "network::ServiceThread\0"
};

const QMetaObject network::ServiceThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_network__ServiceThread,
      qt_meta_data_network__ServiceThread, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &network::ServiceThread::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *network::ServiceThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *network::ServiceThread::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_network__ServiceThread))
        return static_cast<void*>(const_cast< ServiceThread*>(this));
    return QThread::qt_metacast(_clname);
}

int network::ServiceThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_network__ServiceProvider[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   25,   25,   25, 0x0a,
      46,   34,   25,   25, 0x0a,
      92,   34,   25,   25, 0x0a,
     132,   34,   25,   25, 0x0a,
     171,   34,   25,   25, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_network__ServiceProvider[] = {
    "network::ServiceProvider\0\0track()\0"
    "name,sender\0newForeignComputer(std::string,QHostAddress&)\0"
    "foreignFound(std::string,QHostAddress&)\0"
    "changedName(std::string,QHostAddress&)\0"
    "changedAddress(std::string,QHostAddress&)\0"
};

const QMetaObject network::ServiceProvider::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_network__ServiceProvider,
      qt_meta_data_network__ServiceProvider, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &network::ServiceProvider::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *network::ServiceProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *network::ServiceProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_network__ServiceProvider))
        return static_cast<void*>(const_cast< ServiceProvider*>(this));
    return QObject::qt_metacast(_clname);
}

int network::ServiceProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: track(); break;
        case 1: newForeignComputer((*reinterpret_cast< const std::string(*)>(_a[1])),(*reinterpret_cast< QHostAddress(*)>(_a[2]))); break;
        case 2: foreignFound((*reinterpret_cast< const std::string(*)>(_a[1])),(*reinterpret_cast< QHostAddress(*)>(_a[2]))); break;
        case 3: changedName((*reinterpret_cast< const std::string(*)>(_a[1])),(*reinterpret_cast< QHostAddress(*)>(_a[2]))); break;
        case 4: changedAddress((*reinterpret_cast< const std::string(*)>(_a[1])),(*reinterpret_cast< QHostAddress(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
