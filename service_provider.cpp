/*
* Imlementation of service provider which informs other clients and also updates for situation
* Discover and send with udp
* Opens new thread for discovery
*/

#include "service_provider.h"
namespace network{

void network::ServiceThread::run(){
	network::ServiceProvider sdd;
	sdd.setName(name_);
	std::cout << "Thread started!" << std::endl;
	bool enabled = true;
	while(enabled){
		sdd.track();
		sdd.shout();
		QThread::msleep(2000);
	}
}

void network::ServiceThread::setTime(int &time){
	time_ = new int(time);
}

void network::ServiceThread::setName(std::string &name){
	name_ = name;
}

network::ServiceProvider::ServiceProvider(){
	ssock = new QUdpSocket(&handler_);
	ssock->bind(BC_PORT, QUdpSocket::ShareAddress);
	QObject::connect(ssock, SIGNAL(readyRead()), this, SLOT(track()));
}

network::ServiceProvider::~ServiceProvider(){
	ssock->close();
	delete ssock;
}

void network::ServiceProvider::shout(){
	std::string dgram = "[LANDROP]"+name_+"[LANDROP]";
	QByteArray datagram = dgram.c_str();
	ssock->writeDatagram(datagram.data(), datagram.size(), QHostAddress::Broadcast, BC_PORT);
}

void network::ServiceProvider::track(){
	while(ssock->hasPendingDatagrams()){
		QByteArray datagram;
		QHostAddress sender;
		datagram.resize(ssock->pendingDatagramSize());
		ssock->readDatagram(datagram.data(), datagram.size(), &sender);
		QString name = datagram.data();
		name.replace(QString("[LANDROP]"), QString(""));
		if(name.toStdString() != name_){
			std::cout << "Shout heard: " << name.toStdString() << " @ " << sender.toString().toStdString() << std::endl;
			emit foreignFound(name.toStdString(), sender);
		}
	}
}

void network::ServiceProvider::foreignFound(const std::string &name, QHostAddress &sender){
	bool newforeign = true;
	for(unsigned int i=0; i<foreigns.size(); ++i){
		if(foreigns[i].name == name){
			if(foreigns[i].address == sender){
				newforeign = false;
				break;
			}else{
				newforeign = false;
				emit changedAddress(name, sender);
				break;
			}
		}else if(foreigns[i].address == sender){
			newforeign = false;
			emit changedName(name, sender);
			break;
		}
	}
	if(newforeign){
		emit newForeignComputer(name, sender);
	}
}

void network::ServiceProvider::changedAddress(const std::string &name, QHostAddress &sender){
	std::cout << "plop" << std::endl;
}

void network::ServiceProvider::changedName(const std::string &name, QHostAddress &sender){
		std::cout << "plop" << std::endl;
}

void network::ServiceProvider::newForeignComputer(const std::string &name, QHostAddress &sender){
		std::cout << "plop" << std::endl;
}

void network::ServiceProvider::setName(const std::string &name){
	name_ = name;
}

}