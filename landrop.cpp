/*
* LanDrop
* Easy way to share files in lan. Project management.
* Author: Teemu Heikkilä <teemu.heikkila@pistoke.org>
*/

#include "service_provider.h"
#include "sql_controller.h"
#include "filemapper.h"
#include <string>
#include <QApplication>

int main(int argc, char *argv[]){
	QApplication landrop(argc, argv);
	std::string name = "maki";
	std::string path = "/";
	if(argc == 3){
		name = argv[1];
		path = argv[2];
	}
	sql::Database db;
	db.open();
	if(db.init()){
		std::cout << "Database initializing succeed" << std::endl;
	}else{
		std::cout << "Database initializing failed" << std::endl;
		std::cout << db.lastError() << std::endl;
	}
	file::Filemapper fmap(path, &db);
	std::cout << "Added " << db.getEntryCount() << " entries to the database" << std::endl;
	db.close();
	network::ServiceThread t1;
	t1.setName(name);
	t1.setParent(&landrop);
	t1.start();
	return landrop.exec();
}