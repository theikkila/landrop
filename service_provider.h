/*
* Imlementation of service provider which informs other clients and also updates for situation
* Discover and send with udp
* Opens new thread for discovery
*/
#ifndef SERVICE_PROVIDER_H
#define SERVICE_PROVIDER_H

#include <QtNetwork>
#include <QThread>
#include <QObject>
#include <iostream>
#include <string>
#include <vector>

#define BC_PORT 47000

namespace network{
	struct Computer{
		std::string name;
		QHostAddress address;
	};

	class ServiceThread : public QThread{
		Q_OBJECT
	protected:
		void run();
	public:
		void setTime(int &time);
		void setName(std::string &name);
	private:
		int *time_;
		std::string name_;
	};
	
	class ServiceProvider : public QObject{
		Q_OBJECT
	public:
		ServiceProvider();
		~ServiceProvider();
		void shout();
		void setName(const std::string &name);
		
	public slots:
		void track();
		void newForeignComputer(const std::string &name, QHostAddress &sender);
		void foreignFound(const std::string &name, QHostAddress &sender);
		void changedName(const std::string &name, QHostAddress &sender);
		void changedAddress(const std::string &name, QHostAddress &sender);
		
	private:
		std::string name_;
		QUdpSocket *ssock;
		QObject handler_;
		std::vector<Computer> foreigns;
	};

}
#endif