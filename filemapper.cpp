/*
* Implementation of filemapper
*/

#include "filemapper.h"


file::Filemapper::Filemapper(const std::string &path, sql::Database *db){
	QString qpath(path.c_str());
	root_ = QDir(qpath);
	dbptr = db;
	dbptr->open();
	if(!root_.exists()){
		qWarning("Cannot find the directory!");
	}else{
		QDir::setCurrent(qpath);
	}
	searchAndPopulate(path);
}

void file::Filemapper::searchAndPopulate(const std::string &path){
	QString qpath(path.c_str());
	file::Type itemtype = FILE;
	QDir dir(qpath);
	dir.setFilter(QDir::Hidden | QDir::Files | QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);
	dir.setSorting(QDir::Size | QDir::Reversed);
	
	QFileInfoList flist = dir.entryInfoList();
	for(int i=0; i < flist.size(); ++i){
		QFileInfo iteminfo = flist.at(i);
		quint16 hash = 0;
		if(iteminfo.isDir()){
			searchAndPopulate(iteminfo.filePath().toStdString());
			itemtype = FOLDER;
			//std::cout << "Folder: ";
		}else{
			itemtype = FILE;
			hash = getHash(iteminfo.filePath().toStdString());
			//std::cout << "File: ";
		}
		dbptr->addFile(itemtype, iteminfo.fileName().toStdString(), iteminfo.path().toStdString(), iteminfo.lastModified().toString(QString("yyyy-MM-dd hh:mm:ss.zzz")).toStdString(), hash);
		//std::cout << iteminfo.fileName().toStdString() << std::endl;
	}
}

quint16 file::Filemapper::getHash(const std::string &path){
	QString qpath(path.c_str());
	QFile file(qpath);
	if(!file.open(QFile::ReadOnly)){
		std::cout << "Error when opening file " << path << std::endl;
		return 0;
	}
	QByteArray bytes = file.readAll();
	return qChecksum(bytes.data(), bytes.count());
}